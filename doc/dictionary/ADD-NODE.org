#+OPTIONS: *:nil
** ADD-NODE (generic function)
 
: (BUFFER NODE &OPTIONAL X Y (Z) REVIVE-P)
Add the mode NODE to the BUFFER.
Optionally set the location with X,Y,Z.
