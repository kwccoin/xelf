#+OPTIONS: *:nil
** CENTER-POINT (generic function)
 
: (NODE)
Return as multiple values the coordinates of the NODE's center point.
