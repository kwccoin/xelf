#+OPTIONS: *:nil
** *COPYRIGHT-NOTICE* (variable)
Copyright notices for Xelf, its dependencies, and the current Lisp
implementation.
