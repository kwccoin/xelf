#+OPTIONS: *:nil
** FIND-DIRECTION (function)
 
: (X1 Y1 X2 Y2)
Return the heading (in radians) of the ray from Y1,X1 to Y2,X2.
