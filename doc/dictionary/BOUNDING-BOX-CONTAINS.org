#+OPTIONS: *:nil
** BOUNDING-BOX-CONTAINS (function)
 
: (BOX0 BOX1)
Test whether BOX0 contains BOX1. The bounding boxes are provided as
lists of the form (TOP LEFT RIGHT BOTTOM).
