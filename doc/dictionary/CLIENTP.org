#+OPTIONS: *:nil
** CLIENTP (generic function)
 
: (BUFFER)
Return non-nil if this is the Netplay client.
