#+OPTIONS: *:nil
** *BOLD* (variable)
Resource name of default boldface font.
