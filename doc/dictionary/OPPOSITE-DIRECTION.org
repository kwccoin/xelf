#+OPTIONS: *:nil
** OPPOSITE-DIRECTION (function)
 
: (DIRECTION)
Return the direction keyword that is the opposite direction from
DIRECTION.
