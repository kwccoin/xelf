#+OPTIONS: *:nil
** *XELF-VERSION* (variable)
A string giving the version number of Xelf.
