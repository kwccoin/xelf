#+OPTIONS: *:nil
** *MONOSPACE* (variable)
Name of the default monospace (fixed-width) font.
