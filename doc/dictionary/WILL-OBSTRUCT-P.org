#+OPTIONS: *:nil
** WILL-OBSTRUCT-P (generic function)
 
: (NODE PATH-FINDER)
Returns non-nil when the node NODE will obstruct
PATH-FINDER during path finding.
