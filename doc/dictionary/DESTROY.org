#+OPTIONS: *:nil
** DESTROY (generic function)
 
: (NODE)
Destroy this NODE and remove it from any buffers.
