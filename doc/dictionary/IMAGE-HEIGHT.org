#+OPTIONS: *:nil
** IMAGE-HEIGHT (function)
 
: (IMAGE)
Return the height in pixels of IMAGE.
