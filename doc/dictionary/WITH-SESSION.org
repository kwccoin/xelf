#+OPTIONS: *:nil
** WITH-SESSION (macro)
 
: (&BODY BODY)
Starts up Xelf, runs the BODY forms, and starts the main game loop.
Xelf will exit after the game loop terminates.
