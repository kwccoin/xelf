#+OPTIONS: *:nil
** KEYBOARD-RELEASED-P (function)
 
: (KEY)
Returns t if KEY has just been released in the current game loop.
