#+OPTIONS: *:nil
** *OUTPUT-CHUNKSIZE* (variable)
Buffer size. Affects latency.
