(defun snarf-block ()
  (interactive)
  (insert "#+begin_src lisp\n")
  (yank)
  (insert "#+end_src\n\n"))

(defun split-block ()
  (interactive)
  (insert "#+end_src\n\n#+begin_src lisp\n"))

(require 'rx)

(defun replace-defmethods ()
  (interactive)
  (query-replace-regexp (rx
			 "(define-method"
			 (one-or-more space)
			 (group (one-or-more (or "-" (one-or-more (syntax word)))))
			 (one-or-more space)
			 (group (one-or-more (or "-" (one-or-more (syntax word)))))
			 (one-or-more space)
			 "("
			 )
		       "(defmethod \\1 ((self \\2) "))

(defun replace-fields ()
  (interactive)
  (query-replace-regexp (rx
			 "%"
			 (group (one-or-more (or "-" (one-or-more (syntax word)))))
			 )
		       "(slot-value self '\\1)"))

(defun replace-inputs ()
  (interactive)
  (query-replace-regexp (rx
			 "%%"
			 (group (one-or-more (or "-" (one-or-more (syntax word)))))
			 )
		       "(%%inputs self '\\1)"))
  
;; (defun checkdoc-next-block ()
;;   (interactive)
;;   (when (search-forward "#+begin_src lisp" nil t)
;;     (forward-line)
;;     (beginning-of-line)
;;     (org-edit-special)
;;     (let (buffer-file-name)
;;       (checkdoc-defun t)
		  
