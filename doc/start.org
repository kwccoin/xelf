#+TITLE: Getting started with Xelf
#+INFOJS_OPT: view:info mouse:underline up:index.html home:http://xelf.me toc:t  ftoc:t ltoc:t

* About this document  

#+BEGIN_QUOTE  
Use the N and P keys to flip to the Next and Previous pages, or click
the links in the header. Press B to go Back or "?" for help. This
document can also be browsed as a [[file:start-flat.html][single large web page]].
#+END_QUOTE

This is the Getting Started guide for the Xelf game engine.

Xelf supports 2-D games with smooth scrolling, efficient collision
detection between game objects having axis-aligned bounding boxes,
custom collision detection, A-star pathfinding, TrueType fonts, basic
OpenGL blending modes, and more. Future versions will support 3-D
gameplay. Xelf currently runs on Linux, Windows, and Mac. Android
support is also being worked on.

In Xelf you can create active game objects called "nodes"; nodes can
move and interact by being grouped into "buffers". Node and buffer
classes can be subclassed to implement game logic and override default
behavior. 

** Known issues:  

 - Current stable version only supports SDL 1.2 and legacy
   OpenGL. Please see the [[file:future.html][current roadmap]] for future work.
 - Some things are not documented yet (networking, GUI)
  
* About Common Lisp  
  
Teaching Common Lisp itself is outside the scope of this document.

For more general information on developing games in Common Lisp, see
my work-in-progress [[http://xelf.me/guide.html][guide to game development]].
  
* Getting Started on Microsoft™ Windows™

The easiest way to start developing with Xelf on Windows™ is to install
Shinmera's [[http://shinmera.github.io/portacle][Portacle]], a clever all-in-one bundle including GNU Emacs,
Steel Bank Common Lisp, Git, Quicklisp, and a host of other tools
pre-configured for a one-click installation experience.

#+BEGIN_QUOTE
(Please note that Portacle is beta-release software. Full information
is available at the link above.)
#+END_QUOTE

Actually, there's an extra click or two, because of Xelf. Before you
start Portacle, copy the xelf folder into portacle/projects/, then
grab the DLL's from inside portacle/projects/xelf/contrib/win64/ and
copy them to portacle/lib/

If you're ready to continue, just click Portacle.exe.

#+BEGIN_QUOTE
If you haven't got a Xelf release, you can download it with Git as
follows: once you're within GNU Emacs, hit Alt-X to get the command
line at the bottom of the window, then type "shell" and press ENTER.
This should bring up the Windows™ Command prompt in a
subwindow. Now execute the following commands:
#+END_QUOTE

: cd projects
: git clone http://gitlab.com/dto/xelf.git

Once Xelf and its included DLL's are in place, you're ready to go.
Go to the CL-USER prompt. (If you can't find the CL-USER prompt, try
hitting Alt-X and typing "slime" and pressing ENTER. If you seem to be
stuck at the bottom of the window, press ESCAPE three times to quit.)

Now execute the following:

: (ql:quickload :xelf)

It should take a while to download and compile the Lisp libraries Xelf
needs, and then Xelf itself. 

If this succeeds, you're ready to move on to Step 7 below.

* Getting Started on GNU/Linux
  
You can also use [[http://shinmera.github.io/portacle][Portacle]]! Or for a manual setup, follow the
instructions below.

** 1. Install SDL 1.2 libraries  
  
If you are on GNU/Linux, you must install certain libraries through 
your distributions' software installer: SDL 1.2, SDL-IMAGE, SDL-MIXER, 
SDL-GFX, AND SDL-TTF. You may additionally need to install the 
corresponding SDL-*-DEV versions of those packages. 
 
The exact package names vary between distributions; you may wish to 
use Synaptic to search and find the right ones. Remember that Xelf 
uses SDL 1.2 and not SDL2. 
 
32-bit Windows users can find the relevant SDL 1.2 DLL's in the Xelf 
distribution, in ./xelf/contrib/win32/ 

If you are using a 64-bit version of SBCL or CCL on Windows, you may
wish to obtain the SDL 1.2 DLL's from [[http://libsdl.org][libSDL.org]].
 
** 2. Install GNU Emacs and SBCL (or CCL)
 
On Linux, installing GNU Emacs could be as simple as: 
 
: sudo apt-get install emacs  
 
or you may wish to use Synaptic. 
  
On Windows you will need to visit the [[http://www.gnu.org/software/emacs][GNU Emacs site]]. 
 
Although Linux package managers often have SBCL, it can be a very old 
version. Instead you can download the newest stable version from [[http://sbcl.org][SBCL.org]]. 
 
You can also use CCL, see http://clozure.com

** 3. Install Quicklisp  
      
 - http://www.quicklisp.org/   
  
It takes only a few moments to install Quicklisp, and this is the best  
way to download and install all the Common Lisp libraries that Xelf  
depends on---automatically.  
 
To begin, download https://beta.quicklisp.org/quicklisp.lisp 
and load it with SBCL at the shell: 
 
 : sbcl --load quicklisp.lisp 
 
Read the text that comes up and you'll be prompted to enter this: 
 
:  (quicklisp-quickstart:install) 
 
After installing quicklisp you will see a notice about adding  
Quicklisp to your Lisp startup file with (ql:add-to-init-file).  
  
Doing this will make Quicklisp load automatically when you start  
your Lisp implementation, but it isn't strictly required.  
     
:  (ql:add-to-init-file) 
 
** 4. Install Slime 
 
Start SBCL and enter the following: 
 
: (ql:quickload :quicklisp-slime-helper) 
 
Afterward, you should set up your Emacs to load this Slime as 
mentioned here: 
 
 - https://github.com/quicklisp/quicklisp-slime-helper 
  
In short, add this to your Emacs init file: 
 
:  (load (expand-file-name "~/quicklisp/slime-helper.el")) 
:  (setq inferior-lisp-program "sbcl") 
 
To start slime from within Emacs, hit ALT-X and then type "slime" and 
hit RETURN/ENTER. 
 
For more help on how to configure and start Slime, see the Slime User Manual: 
 
 - https://common-lisp.net/project/slime/doc/html/index.html 
 
** 5. Put Xelf in your Quicklisp folder

:   cd ~/quicklisp/local-projects  
:   tar xvzf xelf-VERSION.tar.gz

*** Note

If you are cloning from git or have already extracted the xelf
directory elsewhere, move (or symlink) it to
~/quicklisp/local-projects.

** 6. Load Xelf 
 
Once Emacs and Slime are started, load and compile Xelf from the REPL: 
 
:   (ql:quickload :xelf) 
  
** 7. Link the example game into Quicklisp 
  
At the shell:   
  
:   cd ~/quicklisp/local-projects 
:   ln -s xelf/plong plong

#+BEGIN_QUOTE
Note: Windows users must copy or move the folder to portacle/projects/
instead, as Windows doesn't support symlinks.
#+END_QUOTE

** 8. Play with the examples

Then in Slime:  
        
:   (ql:quickload :plong)  
:   (plong:plong)  
  
If this works, you're ready to start working with Xelf. 

If you get errors, please report problems to me at [[mailto:dto@xelf.me][dto@xelf.me]]. You
can also visit the channel "#lispgames" on irc.freenode.org.

At this stage you might want to check out my work-in-progress [[http://xelf.me/guide.html][guide to
game development]], which covers the implementation of Plong in more
detail.

*** 8a. Advanced example

In shell: 

:   cd ~/quicklisp/local-projects 
:   ln -s xelf/squareball squareball

#+BEGIN_QUOTE
Note: Windows users must copy or move the folder to portacle/projects/
instead, as Windows doesn't support symlinks.
#+END_QUOTE

Then in Slime: 

:   (ql:quickload :squareball)  
:   (squareball:squareball)  

See also [[file:squareball.html][the game of Squareball]] for source code and documentation.
 
** 9. Making a new project 
 
This section is under construction. In the meantime you can use 
Quickproject to get started, and then use the Plong example as a 
boilerplate. 
 
:  (ql:quickload :quickproject) 
 
See http://www.xach.com/lisp/quickproject/ for documentation on 
Quickproject. In short: 
 
: (quickproject:make-project #p"~/src/myproject/" :depends-on '(xelf)) 
 
In the generated "package.lisp" you will need to add Xelf to the list 
of packages being used: 
 
:   (defpackage #:myproject 
:     (:use #:cl #:xelf)) 
 
In the future this will be handled automatically. 

